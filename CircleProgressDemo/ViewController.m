//
//  ViewController.m
//  CircleProgressDemo
//
//  Created by liuyan on 16/7/18.
//  Copyright © 2016年 liuyan. All rights reserved.
//

#import "ViewController.h"
#import "CustomView.h"
#define kScreenHeight [UIScreen mainScreen].bounds.size.height
#define kScreenWidth [UIScreen mainScreen].bounds.size.width
#define CORLOC(c,g,b,a) [UIColor colorWithRed:(c)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:a]

@interface ViewController ()
@property (nonatomic, strong) CustomView *customView;
@property (nonatomic, strong) CustomView *customView1;
@property (nonatomic, strong) CustomView *customView2;
@property (nonatomic, strong) CustomView *customView3;
@property (nonatomic, assign) CGFloat end;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self uiconfig];
    _end = 90;
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)uiconfig{
    //一个圆带头部
    _customView = [[CustomView alloc] initWithFrame:CGRectMake(0, 30, kScreenWidth / 2, kScreenWidth / 2)];
    [_customView setCusRadius:_customView.frame.size.width * 0.35];
    [_customView setCusLineWidth:5];
    _customView.backgroundColor = [UIColor clearColor];
    _customView.doubleCircle = NO;
    _customView.animation = YES;
    _customView.isHead = YES;
    [_customView setViewForegroundColor:CORLOC(250, 63, 65, 1)];
    [_customView setViewBackgroundColor:CORLOC(246, 246, 246, 1)];
    [_customView setRadiusFont:20];
    [self.view addSubview:_customView];
    //两个圆不带头部
    _customView1 = [[CustomView alloc] initWithFrame:CGRectMake(kScreenWidth / 2, 30, kScreenWidth / 2, kScreenWidth / 2)];
    [_customView1 setCusRadius:_customView.frame.size.width * 0.35];
    [_customView1 setCusLineWidth:5];
    _customView1.backgroundColor = [UIColor clearColor];
    _customView1.doubleCircle = YES;
    _customView1.animation = YES;
    _customView1.isHead = NO;
    [_customView1 setViewForegroundColor:CORLOC(250, 63, 65, 1)];
    [_customView1 setViewBackgroundColor:CORLOC(246, 246, 246, 1)];
    [_customView1 setRadiusFont:20];
    [self.view addSubview:_customView1];
    //一个圆带进度显示
    _customView2 = [[CustomView alloc] initWithFrame:CGRectMake(0, 60 + kScreenWidth / 2, kScreenWidth / 2, kScreenWidth / 2)];
    [_customView2 setCusRadius:_customView.frame.size.width * 0.35];
    [_customView2 setCusLineWidth:5];
    _customView2.backgroundColor = [UIColor clearColor];
    _customView2.doubleCircle = NO;
    [_customView2 setViewForegroundColor:CORLOC(255, 0, 0, 1)];
    [_customView2 setViewBackgroundColor:CORLOC(241, 240, 246, 1)];
    [_customView2 setRadiusFont:20];
    _customView2.animation = YES;
    [self.view addSubview:_customView2];
    //不带动画
    //两个圆不带头部
    _customView3 = [[CustomView alloc] initWithFrame:CGRectMake(kScreenWidth / 2, 60 + kScreenWidth / 2, kScreenWidth / 2, kScreenWidth / 2)];
    [_customView3 setCusRadius:_customView.frame.size.width * 0.35];
    [_customView3 setCusLineWidth:5];
    _customView3.backgroundColor = [UIColor clearColor];
    _customView3.doubleCircle = YES;
    _customView3.animation = YES;
    _customView3.isHead = YES;
    [_customView3 setViewForegroundColor:CORLOC(250, 63, 65, 1)];
    [_customView3 setViewBackgroundColor:CORLOC(246, 246, 246, 1)];
    [_customView3 setRadiusFont:20];
    [self.view addSubview:_customView3];

}

- (IBAction)buttonAction:(UIButton *)sender {
    if (_end > 360) {
        _end = 90;
    }
    
    [_customView setStartAngle:0 toEndAngle:_end byRatio:nil byAnimationTime:1.0];
    [_customView1 setStartAngle:0 toEndAngle:_end byRatio:nil byAnimationTime:1.0];
    [_customView2 setStartAngle:0 toEndAngle:_end byRatio:[NSString stringWithFormat:@"%f", 100 * _end / 360 ] byAnimationTime:1.0];
    [_customView3 setStartAngle:0 toEndAngle:_end byRatio:[NSString stringWithFormat:@"%f", 100 * _end / 360 ] byAnimationTime:1.0];

    _end += 90;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
